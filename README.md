# 12123BOOK

**ขั้นตอนการติดตั้งโปรแกรม 12123BOOK**

1. ให้ทำการติดตั้ง xampp
2. ทำการดาวน์โหลดไฟล์ทั้งหมดในนี้รวมเป็น 1 โฟลเดอร์ โดยทำการเซฟไว้ที่  C:\xampp\htdocs โดยตั้งชื่อว่า  12123book
3. เปิด xampp และกด Start Apache และ MYSQL 
4. เปิด web broeser และพิมพ์ localhost
    *( หน้าเว็บจะเข้าไปที่หน้า localhost อัตโนมัต )*
5. เข้าไปที่ phpMyAdmin
6. สร้าง database และ import ไฟล์ database ที่ชื่อ bookonline.sql 
7. เปิดหน้าเว็บโดยการพิมพ์  localhost/12123book/index.php