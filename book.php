<?php
    include('sever.php');
    session_start();
    
        
 

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $_SESSION['title']; ?></title>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style>
    @import url('https://fonts.googleapis.com/css2?family=Fjalla+One&display=swap');

     @font-face {
        font-family: 'Fjalla One', sans-serif;
    }
    body {
        font-family: 'Fjalla One', sans-serif;
    }
    </style>

</head>

<body  >
<div class="header">
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-blue border-bottom shadow-sm">
<h1 class="my-0 mr-md-5 " ><a href="/12123book/index.php" style="color: #ffffff" >12123BOOK</a></h1>

      <form class="form-inline my-0 mr-md-auto" action="/12123book/book_db.php" method="get" >
        <input class="form-control mr-sm-2" style="color: #3c763d" type="text" name="bookname" placeholder="Search" >
        <button class="btn btn-success my-1 my-sm-0" type="submit" id="search" name="search">Search</button> 
      </form>
      
  <nav class="my-2 my-md-0 mr-md-3">
    <a class="p-2 text-dark"> </a>
    <a class="p-2 text-dark"> </a>
   
  </nav>

    <a class="p-2 text-dark" ></a>
    <a class="py-2" href="/12123book/buy.php?text1=<?php echo $username ?>" aria-label="Product">
  <svg class="bi bi-bag" width="2em" style="color: #3c763d" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" d="M14 5H2v9a1 1 0 001 1h10a1 1 0 001-1V5zM1 4v10a2 2 0 002 2h10a2 2 0 002-2V4H1z" clip-rule="evenodd"/>
    <path d="M8 1.5A2.5 2.5 0 005.5 4h-1a3.5 3.5 0 117 0h-1A2.5 2.5 0 008 1.5z"/>
  </svg></a>
</div>
</div>




  <div class="container">
  <div class="row">
    <div class="col">
      
    </div>
    <div class="col">
      
    </div>
  </div>
  <div class="row">
  <a class="p-2 text-dark" ></a>
    
    <div class="picbook">
        <div class="row" style="margin-top: 50px">
            <img src = "/12123book/bookpic/<?php echo $_SESSION['title']; ?>.jpg" width="325" height="450">
        </div>
    </div>

    
    <div class="row align-items-center" style="margin-left: 100px">
    <div class="col" >
       
        <h3 style="color: #3c763d" >ชื่อเรื่อง:  <?php echo $_SESSION['title']; ?></h3>
        <a class="p-2 text-dark"> </a>
        <h4 style="color: #3c763d">ผู้แต่ง: <?php echo $_SESSION['author']; ?></h4>
        <a class="p-2 text-dark"> </a>
        <h4 style="color: #3c763d">ราคา: <?php echo $_SESSION['price']; ?></h4>
        <a class="p-2 text-dark"> </a>

      <?php
          $bookname = $_SESSION['title'];
          $sql1 = "SELECT * FROM book WHERE Title = '$bookname' AND Status = 'valid' ";
          $res = mysqli_query($conn, $sql1);
          $result = mysqli_fetch_assoc($res);  
      ?>


        <h4 style="color: #3c763d">สถานะ: <?php if ($_SESSION['status'] == 'valid'){ echo "พร้อมบริการ"; ?>

        <a style="margin-left: 100px " href="/12123book/buy.php?text=<?php echo $result['BookID']; ?>" class="btn btn-success" name="order"><h3>สั่งซื้อ</h3></a> 

        <?php }else{ echo "<font color=\"red\">หมด</font>" ; ?>
        
        <a style="margin-left: 100px "  class="btn btn-secondary" name="order"><h3 style="color: #ffffff">สั่งซื้อ</h3></a> 

        <?php }  ?></h4>        
    </div> 
    </div>
    </div>
    </div> 
    </div>
</div>
</body>

<script>
  <?php if ($_SESSION['errors1'] == 'belogin') { ?>

    swal("คุณต้องเข้าสู่ระบบก่อนจึงจะสั่งซื้อหนังสือได้", {
      buttons: [   , true],
    });

  <?php }   $_SESSION['errors1'] == 'no' ?>

</script>


</html>
