<?php
    include('sever.php');
    session_start();

    if (isset($_GET['logout'])) {
      session_destroy();
      unset($_SESSION['username']);
      header('location: /12123book/index.php');
    }
    $username = $_SESSION['username'];
    $name = $_SESSION['name'];
    $addr = $_SESSION['addr'];
    $phone = $_SESSION['phone'];
    $point =  $_SESSION['reward'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ข้อมูลสมาชิก</title>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <style>
    @import url('https://fonts.googleapis.com/css2?family=Fjalla+One&display=swap');

     @font-face {
        font-family: 'Fjalla One', sans-serif;
    }
    body {
        font-family: 'Fjalla One', sans-serif;
    }
    </style>

</head>

<body  >
<div class="header">
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-blue border-bottom shadow-sm">
<h1 class="my-0 mr-md-5 " ><a href="/12123book/index.php" style="color: #ffffff" >12123BOOK</a></h1>

      <form class="form-inline my-0 mr-md-auto" action="/12123book/book_db.php" method="get" >
        <input class="form-control mr-sm-2" style="color: #3c763d" type="text" name="bookname" placeholder="Search" >
        <button class="btn btn-success my-1 my-sm-0" type="submit" id="search" name="search">Search</button> 
      </form>
      
  <nav class="my-2 my-md-0 mr-md-3">
    <a class="p-2 text-dark"> </a>
    <a class="p-2 text-dark"> </a>
    <a href="/12123book/transfer.php"><input type="button" class="btn btn-outline-success" value="แจ้งโอนเงิน"></a>
    <a class="p-2 text-dark"> </a>
    <a href="/12123book/member.php?logout='1'" style="color: #3c763d">Log out</a>
  </nav>
    <a class="py-2" href="/12123book/inmem_db.php?inmem='1'"  aria-label="Product">
        <svg class="bi bi-person-square" width="2.5em" style="color: #ffffff" height="2.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M14 1H2a1 1 0 00-1 1v12a1 1 0 001 1h12a1 1 0 001-1V2a1 1 0 00-1-1zM2 0a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2H2z" clip-rule="evenodd"/>
        <path fill-rule="evenodd" d="M2 15v-1c0-1 1-4 6-4s6 3 6 4v1H2zm6-6a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd"/>
        </svg>
    </a>
    <a class="p-2 text-dark" ></a>
    <a class="py-2" href="/12123book/buy.php?<?php echo $username ?>" aria-label="Product">
    <svg class="bi bi-bag" width="2em" style="color: #3c763d" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" d="M14 5H2v9a1 1 0 001 1h10a1 1 0 001-1V5zM1 4v10a2 2 0 002 2h10a2 2 0 002-2V4H1z" clip-rule="evenodd"/>
    <path d="M8 1.5A2.5 2.5 0 005.5 4h-1a3.5 3.5 0 117 0h-1A2.5 2.5 0 008 1.5z"/>
  </svg></a>
</div>
</div>


<div class="container">
  <div class="row">
    <div class="col-sm">
    <h1 class="text-center" style="color: #3c763d">ข้อมูลสมาชิก</h1>
    <h4 class="text-center" style="color: #3c763d">คุณ <?php echo $_SESSION['name'];  ?></h4>
    </div>
  </div>
  </div>
  <div class="row">
    <div class="col-sm">
    <h4 class="text-center" style="color: #3c763d" >MemberID : <?php echo $_SESSION['MID']; ?></h4>
    </div>
  </div>
  </div>

  <div class="container">
  <div class="row">
    <div class="col">
      
    </div>
    <div class="col">
      
    </div>
  </div>
  <div class="row">
      
    <div class="col">
        <svg class="bi bi-person-fill" width="20em" height="20em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd"/>
        </svg>
    </div>
    
    <div class="row align-items-center"> </div>
    <div class="col" style="margin-left: 250px" >
    <div class="list-group" style="margin-left: -200px" >
    <div class="list-group-item list-group-item-action"  style="margin-top: 50px">
    <div class="d-flex w-500 justify-content-between">
        <h5 style="color: black" >ชื่อ: <?php echo "<font color=\"#3c763d\">$name</font>";?> <br>
        <br>ที่อยู่: <?php echo "<font color=\"#3c763d\">$addr</font>";?><br>
        <br>เบอร์โทรศัพท์: <?php echo "<font color=\"#3c763d\">$phone</font>";?><br>
        <br>คะแนนแต้มสะสม: <?php echo "<font color=\"#3c763d\">$point</font>";?></h5>
   
    </div>
    </div>
    </div>
    </div>
    <div class="col">

    </div>
    </div>
</div>

</body>
</html>