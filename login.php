<?php
    include('sever.php');
    session_start();

    if (isset($_SESSION['username'])) {
        header('location: /12123book/member.php');
      }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
   <style>
    @import url('https://fonts.googleapis.com/css2?family=Fjalla+One&display=swap');

     @font-face {
        font-family: 'Fjalla One', sans-serif;
    }
    body {
        font-family: 'Fjalla One', sans-serif;
    }
    </style>

</head>

<body class="text-center" >

<div class="header">
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-blue border-bottom shadow-sm">
<h1 class="my-0 mr-md-5 " ><a href="/12123book/index.php" style="color: #ffffff" >12123BOOK</a></h1>

      <form class="form-inline my-0 mr-md-auto" action="/12123book/searchdb.php" method="get" >
        <input class="form-control mr-sm-2" style="color: #3c763d" type="text" name="bookname" placeholder="Search" >
        <button class="btn btn-success my-1 my-sm-0" type="submit" id="search" name="search">Search</button> 
      </form>
      
  <nav class="my-2 my-md-0 mr-md-3">
    <a class="p-2 text-dark"> </a>
    <a class="p-2 text-dark"> </a>
    <a href="/12123book/transfer.php"><input type="button" class="btn btn-outline-success" value="แจ้งโอนเงิน"></a>
  </nav>

    <a class="p-2 text-dark" ></a>
    <a class="py-2" href="/12123book/buy.php" aria-label="Product">
  <svg class="bi bi-bag" width="2em" style="color: #3c763d" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" d="M14 5H2v9a1 1 0 001 1h10a1 1 0 001-1V5zM1 4v10a2 2 0 002 2h10a2 2 0 002-2V4H1z" clip-rule="evenodd"/>
    <path d="M8 1.5A2.5 2.5 0 005.5 4h-1a3.5 3.5 0 117 0h-1A2.5 2.5 0 008 1.5z"/>
  </svg></a>
</div>
</div>


<div class="container" >
  <!-- Stack the columns on mobile by making one full-width and the other half-width -->
  <div class="row align-items-start">
    <div class="col-md-8"></div>
    <div class="col-6 col-md-4"></div>
  </div>

  <!-- Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop -->
  <div class="row align-items-center">
    <div class="col-6 col-md-4"></div>
    <div class="col-6 col-md-4">
    
<form action="/12123book/login_db.php" method="post">
<h1 style="color: #3c763d">LOG IN</h1>

  <div class="form-group">
    <label for="inputusername"></label>
    <input type="text" class="form-control" id="username" name="username" placeholder="Username" required>
  </div>
  <div class="form-group">
    <label for="inputpassword"></label>
    <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
  </div>
  
    <button type="submit" class="btn btn-success" name="loginuser">Log in</button>
    <a href="/12123book/index.php"><input type="button" class="btn btn-outline-success" value="ยกเลิก"></a>

</form>
    
    </div>
    <div class="col-6 col-md-4"></div>
  </div>

  <!-- Columns are always 50% wide, on mobile and desktop -->
  <div class="row align-items-end">
    <div class="col-6"></div>
    <div class="col-6"></div>
  </div>
</div>
</body>



<script>
<?php if ($_SESSION['errors'] == true) { ?>
  swal("log in ไม่สำเร็จ", "Username หรือ Password ไม่ถูกต้อง!", "warning");
<?php }  $_SESSION['errors'] = false; ?>

</script>

</html>
