<?php
    include('sever.php');
    session_start();
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> 
    <link rel="stylesheet" href="style.css">
    
    <style>
    @import url('https://fonts.googleapis.com/css2?family=Fjalla+One&display=swap');

     @font-face {
        font-family: 'Fjalla One', sans-serif;
        
    }
    body {
        font-family: 'Fjalla One', sans-serif;
    }
    </style>

</head>
<body >

<div class="header">
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-blue border-bottom shadow-sm">
<h1 class="my-0 mr-md-5 " ><a href="/12123book/index.php" style="color: #ffffff" >12123BOOK</a></h1>

      <form class="form-inline my-0 mr-md-auto" action="/12123book/searchdb.php" method="get" >
        <input class="form-control mr-sm-2" style="color: #3c763d" type="text" name="bookname" placeholder="Search" >
        <button class="btn btn-success my-1 my-sm-0" type="submit" id="search" name="search">Search</button> 
      </form>
      
  <nav class="my-2 my-md-0 mr-md-3">
    <a class="p-2 text-dark"> </a>
    <a class="p-2 text-dark"> </a>
    <a href="/12123book/transfer.php"><input type="button" class="btn btn-outline-success" value="แจ้งโอนเงิน"></a>
  </nav>

    <a class="p-2 text-dark" ></a>
    <a class="py-2" href="/12123book/buy.php" aria-label="Product">
  <svg class="bi bi-bag" width="2em" style="color: #3c763d" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" d="M14 5H2v9a1 1 0 001 1h10a1 1 0 001-1V5zM1 4v10a2 2 0 002 2h10a2 2 0 002-2V4H1z" clip-rule="evenodd"/>
    <path d="M8 1.5A2.5 2.5 0 005.5 4h-1a3.5 3.5 0 117 0h-1A2.5 2.5 0 008 1.5z"/>
  </svg></a>
</div>
</div>


<div class="container" class="col-center">
  <div class="row" style="margin-top: 50px">
    <div class="col">
     
    </div>
    <div class="col-6">
      <h1 class="text-center" style="color: #3c763d">สมัครสมาชิก</h1>
    </div>
    <div class="col">
     
    </div>
  </div>
  <div class="row">
    <div class="col">
     
    </div>
    <div class="col-5">
      
    </div>
    <div class="col">
      
    </div>
  </div>
</div>


    <form action="/12123book/register_db.php" method="post">
    <?php// include('errors.php'); ?>
        <div class="container">
            <div class="row" style="margin-top: 75px">
                <div class="col">
                <div class="form-group">
                    <label for="name">ชื่อ-นามสกุล<a style="color: red">*</a></label>
                    <input type="text" class="form-control"  name="name" placeholder="ใส่ชื่อ-นามสกุล" required>
                </div>
                </div>
                <div class="col">
                <div class="row">
                    <div class="col">
                    <label for="username"><small>Username<a style="color: red">*</small></a></label>
                    <input type="text" class="form-control" name="username" placeholder="Username" required>
                    </div>
                    <div class="col">
                    <label for="password"><small>Password<a style="color: red">*</small></a></label>
                    <input type="password" class="form-control" name="password" placeholder="Password" required>
                    </div>
                </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                <div class="row">
                    <div class="col">
                    <label for="citizen">เลขประจำตัวประชาชน<a style="color: red">*</a></label>
                    <input type="text" class="form-control" name="citizen" placeholder=" x-xxxx-xxxxx-xx-x " required>
                    </div>
                    <div class="col">
                    <label for="phone">เบอร์โทรศัพท์<a style="color: red">*</a></label>
                    <input type="tel" class="form-control" name="phone"  required>
                    </div>
                </div>
                </div>
                <div class="col">
                <div class="form-group">
                    <label for="address">ที่อยู่<a style="color: red">*</a><small>(สำหรับจัดส่งหนังสือ)</small></label>
                    <textarea class="form-control" name="address" rows="3" required></textarea>
                </div>
                </div>
            </div>
            <div class="row" >
           
                <div class="col">
           
                </div>
                <div class="pull-right">
                <div class="col" > 
                    <a href="/12123book/index.php"><input type="button" class="btn btn-outline-success" value="ยกเลิก"></a>
                    <button  type="submit" name="registeruser" class="btn btn-success">สมัครสมาชิก</button>  
                </div>
            </div>
            </div>
        </div>
    </form>

</body>
</html>